'use server';

import { cookies } from 'next/headers';

type createCokiesProps = {
    nameCookies: string,
    valueCookie: string
}

export async function createCokies( nameCookies:string, valueCookie: string) {
    cookies().set({
        name: nameCookies,
        value: valueCookie,
        httpOnly: true,
        path: '/',
      });    
}