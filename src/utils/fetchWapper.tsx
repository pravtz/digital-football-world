export async function fetchWrapper<T = unknown>(
    input: RequestInfo | URL,
    init?: RequestInit | undefined
) {
    const data = await fetch(`https://v3.football.api-sports.io/${input}`, init)
    const result = await data.json()
    return result as T
}