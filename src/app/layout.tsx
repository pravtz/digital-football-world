import './globals.css'
import { Inter } from 'next/font/google'
import { clsx } from 'clsx';
import { Heart, Coffee } from 'lucide-react'

const inter = Inter({ subsets: ['latin'], variable: "--font-inter" })

export const metadata = {
  title: 'Digital Football World',
  description: 'O App definitivo para os apaixonados por futebol!',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body
        className={clsx(
          inter.variable,
          'bg-gradient-to-b from-bg-default to-primary',
          'min-h-screen w-full overflow-auto flex flex-col justify-between'
        )}>

        <main className=' font-sans text-text-default  h-full'>
          {children}
        </main>
        <footer>
          <p
            className={clsx('opacity-90 my-2 flex items-center font-sans text-sm gap-2 text-text-default w-full justify-center')}
          >Feito com muito
            <Heart size={18} className='text-red-500' />
            e
            <Coffee size={18} className='text-text-default' /> por
            <a href='http://pravtz.dev.br' className=' hover:underline font-bold cursor-pointer hover:underline-offset-4 ' target="_blank" rel="noopener noreferrer">Ederson Pravtz</a>
          </p>
        </footer>
      </body>
    </html>
  )
}
