import { api } from '@/lib/api'
import { NextRequest, NextResponse } from 'next/server'


export async function GET(request: NextRequest) {

    const { searchParams } = new URL(request.url)
    const token = searchParams.get('code')
    const cookieExpiresInSeconds = 60 * 60 * 24 * 30

    if (token) {
        try {
            const statusResponse = await api.get('/status', {
                headers: {
                    "x-rapidapi-host": "v3.football.api-sports.io",
                    "x-rapidapi-key": token
                }

            })
            if (statusResponse.data.response?.subscription.active) {

                const redirectURL = new URL('/home', request.url)
                return NextResponse.redirect(redirectURL, {
                    headers: {
                        'Set-Cookie': `token=${token}; Path=/; max-age=${cookieExpiresInSeconds};`,
                    },
                })
            } else {
                const redirectURL = new URL('/?error=keyInvalid', request.url)
                return NextResponse.redirect(redirectURL)
            }

        } catch (error) {
            console.log('error:', error)
            const redirectURL = new URL('/?error=ConectError', request.url)

            return NextResponse.redirect(redirectURL)
        }

    } else {
        const redirectURL = new URL('/', request.url)
        return NextResponse.redirect(redirectURL, {
            headers: {
                'Set-Cookie': `token=; Path=/; `,
            },
        })
    }

}
