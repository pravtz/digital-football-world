
import Image from 'next/image'
import LogoA from '@/assets/brands/logo-digital.svg'
import LogoB from '@/assets/brands/logo2.svg'
import { Auth } from '../components/Auth'
import { cookies } from "next/headers";

export default async function Home() {
  const tokenKey = cookies().get('token')


  return (
    <>
      <section className='flex flex-col justify-center h-[94vh] '>
        <div className='flex flex-col w-full h-full   items-center justify-center '>
          <div className='relative h-fit  '>

            <div className='absolute bg-tertiary rounded-full w-[200px] h-[200px] blur-[80px] left-1/2 -translate-x-1/2 opacity-50 -z-10 ' ></div>

            <div className='flex flex-col md:flex-row   md:min-w-[800px]'>

              <div className='w-[119px] md:w-[402px] md:h-[402px] h-[119px] m-auto  relative'>
                <Image className='object-contain' fill alt='logo' src={LogoB} />
              </div>

              <div className='flex flex-col   max-w-[399px] px-1 sm:px-0'>
                <div className='mx-auto md:mx-0 my-4 relative md:w-[280px] md:h-[141px] w-[181px] h-[80px]'>
                  <Image className='' fill alt='logo' src={LogoA} />
                </div>
                <h2 className=' my-4 text-xl leading-8'>Bem-vindo ao <strong className='text-2xl'>Digital Football World</strong>, o app definitivo para os apaixonados por futebol! Prepare-se para mergulhar em um universo de partidas emocionantes de todo o mundo, tudo na palma da sua mão.</h2>
                <Auth token={tokenKey?.value} />
              </div>

            </div>
          </div>

        </div>
      </section>
    </>

  )
}
