import Image from 'next/image'
import LogoA from '@/assets/brands/logo-digital.svg'
import LogoB from '@/assets/brands/logo2.svg'
import { NavRouterHeader } from '../../components/NavRouteHeader';

export default function PlayLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <section className='flex flex-col justify-start h-full '>
        <div className='container mx-auto'>
          <NavRouterHeader />
        </div>
        <div className='flex flex-row w-full items-center justify-center relative'>
          <div className='absolute bg-tertiary rounded-full w-[200px] h-[200px] blur-[80px] left-1/2 -translate-x-1/2 opacity-50 -z-10 mt-14' />
          <div className='w-[76px] h-[76px] sm:w-[119px] sm:h-[119px] '>
            <Image alt='logo do app que retrata o menino jogando futebol' src={LogoB} />
          </div>
          <div className='w-[145px] h-[64px] sm:w-[209px] sm:h-[92px]'>
            <Image alt='logo do app dizendo o nome do app' src={LogoA} />
          </div>
        </div>
        <div>
          {children}
        </div>
      </section>


    </>
  )
}