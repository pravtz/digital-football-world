import { ScreenChoiceCountryAndSeasons } from "@/components/ScreenChoiceCountryAndSeasons";
import { getDataCountries } from "@/services/v3.football.api-sports.io/getDataCountries";

export default async function ChoiceOfCountry() {
  const data = await getDataCountries()

  return (
    <div className="container m-auto h-full ">
      <ScreenChoiceCountryAndSeasons contriesList={data.response} />
    </div>

  )
}