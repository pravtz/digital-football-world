import { ScreenChoiceLeagues } from "@/components/ScreenChoiceLeagues"
import { ScreenChoiceTeamsOfTheLeagues } from "@/components/ScreenChoiceTeamsOfTheLeagues"
import { ScreenDashboardTeamLeagues } from "@/components/ScreenDashboardTeamLeagues"
import { getDataLeagues } from "@/services/v3.football.api-sports.io/getDataLeagues"
import { getDataPlayersSquads } from "@/services/v3.football.api-sports.io/getDataPlayersSquads"
import { getDataStatistics } from "@/services/v3.football.api-sports.io/getDataStatistics"
import { getDataTeamsLeagues } from "@/services/v3.football.api-sports.io/getDataTeamsLeagues"

export default async function Leagues({ params }: { params: { leagues: string } }) {
    const { leagues } = params
    const amountParams = leagues.length


    if (amountParams === 2) {
        const DataLeagues = await getDataLeagues(leagues[1], leagues[0])

        return <ScreenChoiceLeagues data={DataLeagues} />
    }

    if (amountParams === 3) {
        const DataTeamsLeagues = await getDataTeamsLeagues(leagues[2], leagues[0])
        console.log('DataTeamsLeagues', DataTeamsLeagues)

        return <ScreenChoiceTeamsOfTheLeagues country={leagues[1]} data={DataTeamsLeagues} />
    }

    if (amountParams === 4) {
        const DataDataPlayersSquads = await getDataStatistics(leagues[2], leagues[0], leagues[3])
        return (
            <ScreenDashboardTeamLeagues data={DataDataPlayersSquads} />
        )
    }
    if (amountParams === 5) {
        console.log('leagues ', leagues)
        console.log('amountParams', amountParams)

        return (
            <h1>mostrando o time</h1>
        )
    }

    return (

        <h1>leagues</h1>
    )
}