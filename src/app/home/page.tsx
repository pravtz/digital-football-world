import { ButtonRoutePush } from "@/components/ButtonRoutePush";
import { ProgressBar } from "@/components/ProgressBar";
import { getDataStatus } from "@/services/v3.football.api-sports.io/getDataStatus";

export default async function Play() {
    const data = await getDataStatus()

    const currentValue = data?.response.requests.current
    const fullValue = data.response.requests.limit_day

    return (
        <div className="flex flex-col items-center h-full justify-center ">
            <div className="w-full flex justify-center my-8">
                <ProgressBar currentValueProgress={currentValue} fullValueProgress={fullValue} />
            </div>
            <h4>Olá {data.response.account.fistname}</h4>
            <p>Seu status esta: <span className="font-bold">{data.response.subscription.active ? 'Ativo' : "Não Ativo"}</span></p>
            <p>Seu plano é: <span className="font-bold">{data.response.subscription.plan}</span></p>

            <ButtonRoutePush buttonMessage="Entrar" linkDestination="/home/team" />
        </div>
    )
}