import { BoxPlayer } from "@/components/BoxPlayer"
import { getDataPlayersSquads } from "@/services/v3.football.api-sports.io/getDataPlayersSquads"
import Image from "next/image"


export default async function Teams({ params }: { params: { idteamlayers: string } }) {
    const data = await getDataPlayersSquads(params.idteamlayers)

    const DataResponse = data.response[0]

    return (
        <div className="container mx-auto px-6 ">
            <div className="flex w-full items-center justify-center gap-[50%] mb-8">
                <div className="flex flex-col items-center">
                    <div>
                        <Image src={DataResponse?.team.logo} width={60} height={60} alt={`logo do ${DataResponse?.team.name}`} />
                    </div>
                    <h1 className="font-bold text-xs">{DataResponse?.team.name}</h1>
                </div>

            </div>
            <div className="flex flex-wrap gap-8 ">
                {DataResponse?.players.map((item) => (
                    <BoxPlayer key={item.id} id={item.id} name={item.name} urlPhoto={item.photo} />
                ))}
            </div>
        </div>
    )
}