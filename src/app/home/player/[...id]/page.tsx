import { getDataPlayer } from "@/services/v3.football.api-sports.io/getDataPlayer"
import Image from "next/image"
import { cookies } from "next/headers";


export default async function Teams({ params }: { params: { id: string } }) {
    const tokenKey = cookies().get('token')
    console.log('tokenKey', tokenKey)

    const data = await getDataPlayer(params.id[0], params.id[1])

    const dataplayer = data.response[0]

    return (
        <div className="container m-auto">
            <div className=" rounded-lg bg-primary/10 max-w-[258px] m-auto my-10 p-4">
                <div className="flex flex-col items-center justify-center">

                    <Image className="rounded-full object-cover" src={dataplayer.player.photo} alt={dataplayer.player.name} width={113} height={113} />
                    <p>{dataplayer.player.name}</p>
                </div>
                <div className="flex flex-col gap-1 py-4">
                    <h1 className=" text-xs font-light">Name: <span className="text-sm font-bold">{dataplayer.player.name}</span></h1>
                    <p className=" text-xs font-light">Idade: <span className="text-sm font-bold">{dataplayer.player.age} anos</span></p>
                    <p className=" text-xs font-light">Nacionalidade: <span className="text-sm font-bold">{dataplayer.player.nationality}</span></p>
                </div>
            </div>

        </div>
    )
}