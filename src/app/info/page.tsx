import Image from "next/image";
import imageLoginApiSports from 'src/assets/captureInfo/capture-info-login.png'
import imageGetApiKey from 'src/assets/captureInfo/capture-info-get-api-key.png'
import imageAccessLink from 'src/assets/captureInfo/capture-info-aceess-link.png'

export default async function Info() {
    return (
        <div className="flex flex-col items-center h-full justify-center ">
            <div className="w-full flex flex-col items-center  justify-center  my-8">
                <h1 className="text-2xl font-bold">Saiba como obter a chave de acesso!</h1>
                <div>

                    <p>Para obter a chave de acesso siga os seguintes passos:</p>
                    <ul className="list-decimal space-y-3 my-8">
                        <li>
                            <p>Cadastre uma conta no
                                <a className="underline ring-offset-4 font-bold " href="https://dashboard.api-football.com/register" target="_blank" rel="noopener noreferrer">API sports <span className=" no-underline font-mono text-xs text-white/50">(https://dashboard.api-football.com/register)</span></a>
                            </p>

                            <div className="w-[300px] my-3">
                                <Image src={imageLoginApiSports} alt="tela de login da pagina da API Sports" />
                            </div>
                        </li>

                        <li>
                            <p>Na tela do dashboard, clique no icone Account no lado esquerdo dos icones de navegação. Em seguida ira abrir um menu na esquerda da tela. Clique em
                                <a className="" href="https://dashboard.api-football.com/profile?access">My access</a>
                            </p>
                            <div className="w-[300px]">
                                <Image src={imageAccessLink} alt="tela de login da pagina da API Sports" />
                            </div>
                        </li>

                        <li>
                            <p>No canto superior direiro copie o codigo que aparece no campo (os dados aparecem levemente ofuscado para segurança dos usuarios, no entanto se aproximar o cursor do mouse é possível copiar)</p>
                            <div className="w-[300px]">
                                <Image src={imageGetApiKey} alt="tela de login da pagina da API Sports" />
                            </div>
                        </li>
                        <li>
                            <p>Cole a chave de acesso no campo indicado e clique em entrar</p>
                        </li>
                    </ul>
                </div>


            </div>
        </div>
    )
}