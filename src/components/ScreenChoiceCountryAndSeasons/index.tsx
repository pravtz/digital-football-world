'use client'
import { FormCountryAndSeasons } from "@/components/FormCountryAndSeasons";
import { countries } from "@/services/v3.football.api-sports.io/getDataCountries";
import { useRouter } from "next/navigation"

type contriesListProps = {
    contriesList: countries[]
}

export const ScreenChoiceCountryAndSeasons = ({ contriesList }: contriesListProps) => {
    const { push } = useRouter()

    const handlerSubmit = async (country: string, seasons: string) => {
        push(`/home/team/${seasons}/${country}`)
    }
    return (

        <div className="flex flex-col justify-center items-center">
            <FormCountryAndSeasons countryObj={contriesList} choiceCountryAndSeasons={handlerSubmit} />
        </div>

    )
}