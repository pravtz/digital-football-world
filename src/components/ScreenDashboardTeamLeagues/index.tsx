import { requestStatistics } from "@/services/v3.football.api-sports.io/getDataStatistics"
import Image from "next/image"
import { BoxNumberStatistic } from "../BoxNumberStatistic"
import { ChartGoals } from "../ChartGoals"
import player from '@/assets/player.svg'
import squareStrategi from '@/assets/squareStrategi.svg'
import Link from "next/link"



type ScreenDashboardTeamLeaguesProps = {
    data: requestStatistics,
}

export const ScreenDashboardTeamLeagues = ({ data }: ScreenDashboardTeamLeaguesProps) => {

    const DataResponse = data.response
    const goalsFor = (Object.values(DataResponse.goals.for.minute)).map(item => item.total)
    const goalsAgainst = (Object.values(DataResponse.goals.against.minute)).map(item => item.total)


    return (
        <div className=" container mx-auto p-4">
            <div className="flex w-full items-center justify-center gap-[50%] mb-8">
                <div className="flex flex-col items-center">
                    <div>
                        <Image src={DataResponse.team.logo} width={60} height={60} alt={`logo do ${DataResponse.team.name}`} />
                    </div>
                    <h1 className="font-bold text-xs">{DataResponse.team.name}</h1>
                </div>
                <div className="flex flex-col items-center">
                    <div>
                        <Image src={DataResponse.league.logo} width={60} height={60} alt={`logo do ${DataResponse.league.name}`} />
                    </div>
                    <h1 className="font-bold text-xs">{DataResponse.league.name}</h1>
                </div>
            </div>
            <div className="grid gap-4 grid-cols-2 md:grid-cols-4 my-4">
                <BoxNumberStatistic title="Total de Jogos" value={DataResponse.fixtures.played.total} />
                <BoxNumberStatistic title="Total de vitórias" value={DataResponse.fixtures.wins.total} />
                <BoxNumberStatistic title="Total de Empates" value={DataResponse.fixtures.draws.total} />
                <BoxNumberStatistic title="Total de Derrotas" value={DataResponse.fixtures.loses.total} />
            </div>
            <div className="bg-primary/10 rounded-lg w-[100%] mx-auto">
                <ChartGoals coordYDataFor={goalsFor} coordYDataAgainst={goalsAgainst} coordXLabel={Object.keys(DataResponse.goals.for.minute)} />
            </div >
            <div className="flex gap-4 w-full h-full justify-stretch items-stretch ">
                <Link className="w-full h-full " href={`/home/team-players/${DataResponse.team.id}?season=${data.parameters.season}&leagues=${data.parameters.league}`} >
                    <div className="px-4 py-6 rounded-lg  bg-white/5 hover:bg-white/20 flex flex-col sm:flex-row items-center justify-center  gap-2">
                        <div className="w-[60px] h-[60px]">
                            <Image src={player} alt="icone de um Jogadir chutando uma bola" width={60} height={60} />
                        </div>
                        <p className="font-bold text-2xl">Jogadores</p>
                    </div>
                </Link>

                <button className="w-full h-full" type="button" >
                    <div className="px-4 py-6 rounded-lg bg-white/5 hover:bg-white/20 flex flex-col sm:flex-row items-center justify-center gap-2 ">
                        <div className="w-[60px] h-[60px]">
                            <Image src={squareStrategi} alt="icone de um quadro de estrategias" width={60} height={60} />
                        </div>
                        <p className="font-bold text-2xl">Jogadores</p>
                    </div>
                </button>
            </div>
        </div>
    )
}