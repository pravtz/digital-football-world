type BoxNumberStatisticType = {
    title: string,
    value: number
}

export const BoxNumberStatistic = ({ title, value }: BoxNumberStatisticType) => {


    return (
        <div className="px-4 py-6 rounded-lg bg-primary/10 fle flex-col items-center justify-center ga">
            <h3 className=" text-center font-thin text-xs uppercase">{title}</h3>
            <p className="font-bold text-2xl text-center">{value}</p>
        </div>
    )
}