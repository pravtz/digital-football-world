'use client'
import { Bar } from 'react-chartjs-2';
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
    ChartData,
    ChartOptions,
    ChartDatasetProperties,
} from 'chart.js';

ChartJS.register(
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

const options: ChartOptions<'bar'> = {
    responsive: true,
    scales: {
        y: {
            beginAtZero: true,
            ticks: {
                color: 'white'
            }
        },
        x: {
            ticks: {
                color: 'white'
            }
        }
    },
    plugins: {
        legend: {
            position: 'top' as const,
            labels: {
                color: "#fff"
            }
        },
        title: {
            display: true,
            text: 'Número de Gols Marcados',
            color: "white"
        },




    },
}
type ChartGoalsProps = {
    coordXLabel?: number[] | string[],
    coordYDataFor: (number | [number, number] | null)[]
    coordYDataAgainst: (number | [number, number] | null)[]
}

export const ChartGoals = ({ coordYDataFor, coordXLabel, coordYDataAgainst }: ChartGoalsProps) => {
    const labels = coordXLabel
    const data: ChartData<'bar'> = {
        labels,
        datasets: [
            {
                label: 'Total de gols feitos',
                borderWidth: 1,
                data: coordYDataFor,
                backgroundColor: '#44A890'
            },
            {
                label: 'Total de gols sofridos',
                borderWidth: 1,
                data: coordYDataAgainst,
                backgroundColor: 'red'

            },

        ]
    }
    return (
        <div className=' mx-auto h-[80%]'>
            <Bar options={options} data={data} />
        </div>
    )
}