import Image from "next/image"


type ItemChoiceProps = {
    image: string,
    title: string
}

export const TeamItem = ({ image, title }: ItemChoiceProps) => {
    return (

        <div className=" flex items-center justify-start bg-white/10 rounded-lg p-2 ">
            <div className="w-9 h-9 object-scale-down object-container overflow-hidden">
                <Image src={image} width={36} height={36} alt="title" />
            </div>
            <h2>{title}</h2>
        </div>

    )
}