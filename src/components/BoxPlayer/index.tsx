'use client'
import Link from "next/link"
import Image from "next/image"
import { useSearchParams } from "next/navigation"

type BoxPlayerProps = {
    id: number,
    urlPhoto: string,
    name: string
}


export const BoxPlayer = ({ id, urlPhoto, name }: BoxPlayerProps) => {

    const searchParams = useSearchParams()
    const seasons = searchParams.get('season')
    const leagues = searchParams.get('leagues')
    return (
        <div className=" mx-auto flex flex-col gap-2  items-center bg-tertiary/10  rounded-lg w-[87px]  p-2">
            <Link prefetch={false} href={`/home/player/${id}/${seasons}/${leagues}`}>
                <Image className="object-cover rounded-full" src={urlPhoto} width={71} height={71} alt={`foto do ${name}`} />
                <p className="text-[10px] text-center">{name}</p>
            </Link>

        </div>
    )
}