import { useForm } from "react-hook-form"
import { zodResolver } from '@hookform/resolvers/zod'
import { z } from 'zod'
import clsx from "clsx";
import { ClipboardCopy, X } from "lucide-react";
import Link from "next/link";


const schema = z.object({
    key: z.string().min(24, { message: "key invalido!" }).optional().catch(undefined)
})

type Authenticatetype = z.infer<typeof schema>;

type AutenticateProps = {
    authKey: (key: string | undefined) => void
    isFetching: boolean
    tokenKey?: string
    deleteToken: () => void
}

export const FormAuthenticate = ({ authKey, isFetching, tokenKey, deleteToken }: AutenticateProps) => {


    const { register, handleSubmit, formState: { errors }, reset, setValue } = useForm<Authenticatetype>({
        criteriaMode: 'all',
        mode: 'all',
        resolver: zodResolver(schema),
        values: {
            key: tokenKey
        }
    })


    const onSubmit = async ({ key }: Authenticatetype) => {
        await authKey(key);
        reset();
    }
    const deleteFill = () => {
        deleteToken()
        setValue('key', undefined)
    }
    return (
        <div className="flex flex-col">
            <form className="" onSubmit={handleSubmit(onSubmit)}>
                <div className="w-full">

                    <Link href='/info' className=" my-1" >
                        <p className="text-[14px]  text-right hover:underline underline-offset-2 w-full">Saiba aqui como obter a chave API-KEY</p>
                    </Link>
                    <div className="relative ">
                        <div className="opacity-60 hover:opacity-100 absolute right-4 top-1/2 -translate-y-1/2 cursor-pointer">
                            {tokenKey ? <X onClick={deleteFill} color="#DDD" size={20} /> : <ClipboardCopy color="#DDD" size={20} />}
                        </div>
                        <input
                            className=" rounded-lg w-full h-10 bg-primary placeholder:text-text-default placeholder:opacity-40 px-4 border border-text-default/20"
                            type="text"
                            {...register("key")}
                            placeholder="Entre com sua chave API-Key" />
                    </div>
                </div>
                <button
                    className="w-full active:bg-red-500 rounded-lg bg-text-default text-secondary text-2xl uppercase text-center h-14 px-4 my-4"
                    type="submit"
                    disabled={isFetching}
                >{!isFetching ? "Entrar" : "Aguarde..."}</button>
                <div className={clsx(errors.key?.message ? "block" : "hidden", 'border-[4px] border-[#FF7B7B]/20 rounded-lg bg-[#8D4545]/50 text-[#E29E9E] uppercase p-2 text-[11px]')}>
                    <p>{errors.key?.message}</p>
                </div>
            </form>

        </div>
    )
}