'use client'

import { useEffect, useState } from "react"
import { FormAuthenticate } from "../FormAuthenticate"
import { useRouter } from "next/navigation"

export const Auth = ({ token }: { token?: string }) => {
  const [keyCode, setKeyCode] = useState<string | undefined>(undefined)
  const [isFetching, setIsFetching] = useState<boolean>(false)
  const { push } = useRouter()

  useEffect(() => {
    if (token) {
      setKeyCode(token)
    }
  }, [token])

  const deleteToken = () => {
    setKeyCode(undefined)
    push(`/api/auth/logout`)
  }

  const handlerSubmit = async (key: string | undefined) => {
    setIsFetching(true)
    setKeyCode(key)
    push(`/api/auth/login?code=${key}`)
    setIsFetching(false)
  }

  return (

    <div>
      <FormAuthenticate authKey={handlerSubmit} isFetching={isFetching} tokenKey={keyCode} deleteToken={deleteToken} />
    </div>


  )
}