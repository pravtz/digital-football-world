import { useForm } from "react-hook-form"
import { zodResolver } from '@hookform/resolvers/zod'
import { z } from 'zod'
import { countries } from "@/services/v3.football.api-sports.io/getDataCountries";


const schema = z.object({
    country: z.string(),
    seasons: z.string()
})

type Authenticatetype = z.infer<typeof schema>;



type AutenticateProps = {
    choiceCountryAndSeasons: (country: string, seasons: string) => void
    countryObj: countries[]
}

export const FormCountryAndSeasons = ({ choiceCountryAndSeasons, countryObj }: AutenticateProps) => {

    const { register, handleSubmit, formState: { errors }, reset } = useForm<Authenticatetype>({
        criteriaMode: 'all',
        mode: 'all',
        resolver: zodResolver(schema)
    })



    const onSubmit = async ({ country, seasons }: Authenticatetype) => {
        await choiceCountryAndSeasons(country, seasons);
        reset();
    }

    return (
        <form className="flex flex-col gap-4 w-72 mt-10" onSubmit={handleSubmit(onSubmit)} >
            <p >Selecione:</p>
            <select {...register('country')} className="appearance-none rounded-lg w-full h-10 bg-primary placeholder:text-text-default placeholder:opacity-40 px-4 border border-text-default/20" placeholder="País (country)">
                <option>País (country)</option>
                {countryObj.map((item) => {
                    if (item.code != null) {
                        return (
                            <option key={item.name} value={item.code}>{item.name}</option>
                        )
                    }

                })}

            </select>
            <select {...register('seasons')} className="appearance-none rounded-lg w-full h-10 bg-primary placeholder:text-text-default placeholder:opacity-40 px-4 border border-text-default/20" placeholder="Temporada (seasons)">
                <option>Temporada (seasons)</option>
                <option value={'2008'}>2008</option>
                <option value={'2009'}>2009</option>
                <option value={'2010'}>2010</option>
                <option value={'2011'}>2011</option>
                <option value={'2012'}>2012</option>
                <option value={'2013'}>2013</option>
                <option value={'2014'}>2014</option>
                <option value={'2015'}>2015</option>
                <option value={'2016'}>2016</option>
                <option value={'2017'}>2017</option>
                <option value={'2018'}>2018</option>
                <option value={'2019'}>2019</option>
                <option value={'2020'}>2020</option>
                <option value={'2021'}>2021</option>
                <option value={'2022'}>2022</option>
                <option value={'2023'}>2023</option>
                <option value={'2024'}>2024</option>
                <option value={'2025'}>2025</option>
            </select>
            <button type="submit" className="text-text-default m-8 px-6 py-4  bg-primary/70 hover:bg-primary/100 rounded-lg font-bold text-base">Continuar</button>
        </form>
    )
}