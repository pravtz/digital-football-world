'use client'
import { ArrowLeft, Home, LogOut } from 'lucide-react'
import { useRouter, usePathname } from 'next/navigation';


export const NavRouterHeader = () => {
    const pathname = usePathname()
    const router = useRouter();

    return (
        <nav className='w-full flex items-center justify-between mt-11 px-4 sm:px-0'>
            {pathname === "/home"
                ?
                <button onClick={() => router.push('/')}>
                    <LogOut />
                </button>
                :
                <button onClick={() => router.back()}>
                    <ArrowLeft />
                </button>
            }

            <button onClick={() => router.push('/home')}>
                <Home />
            </button>
        </nav>

    )
}