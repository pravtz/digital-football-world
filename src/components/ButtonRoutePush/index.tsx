'use client'
import { useRouter } from 'next/navigation';

type ButtonRoutePushProps = {
    linkDestination: string
    buttonMessage: string
}

export const ButtonRoutePush = ({ linkDestination, buttonMessage }: ButtonRoutePushProps) => {
    const router = useRouter();
    return (

        <button type='button'
            onClick={() => router.push(linkDestination)}
            className="text-text-default m-8 px-6 py-4  bg-primary/70 hover:bg-primary/100 rounded-lg font-bold text-base"
        >
            {buttonMessage}
        </button>
    )
}