'use client'

import { Search } from 'lucide-react';
import { Suspense, useState } from 'react';
import { requestTeamsLeagues } from '@/services/v3.football.api-sports.io/getDataTeamsLeagues';
import Link from 'next/link';
import { TeamItem } from '../TeamItem';


type ScreenChoiceTeamsOfTheLeaguesProps = {
  data: requestTeamsLeagues
  country: string
}

export const ScreenChoiceTeamsOfTheLeagues = ({ data, country }: ScreenChoiceTeamsOfTheLeaguesProps) => {
  const [search, setSearch] = useState('')

  const responseData = data.response

  const dataFiltred = responseData.filter(value => value.team.name.toLowerCase().indexOf(search.toLowerCase()) !== -1)


  return (
    <div>
      <div className="container mx-auto">
        <div className='w-[80%] m-auto '>
          <div className='flex flex-col my-2 justify-center sm:flex-row sm:justify-between items-center gap-2'>
            <h1 className='text-xl my-4'>Ligas</h1>
            <div className='relative flex items-center'>
              <Search className='absolute right-2' />
              <input onChange={e => setSearch(e.target.value)} autoFocus className='rounded-lg w-full h-10 bg-primary placeholder:text-text-default placeholder:opacity-40 px-4 border border-text-default/20' type="text" id="filter" />
            </div>
          </div>

          <div className='max-h-[53vh] sm:max-h-[61vh] overflow-auto scrollbar scrollbar-thumb-gray-900/20 scrollbar-track-gray-100/15 scrollbar-w-1 scrollbar-rounded-full '>
            <Suspense fallback={
              <p>Loading...</p>
            }>
              <div className='flex flex-col gap-2 snap-y snap-mandatory touch-pan-y'>
                {dataFiltred.map((item) => {
                  return (
                    <div className='snap-always snap-start scroll-py-6 pr-1' key={item.team.id}>
                      <Link prefetch={false} href={`/home/team/${data.parameters.season}/${country}/${data.parameters.league}/${item.team.id}`}>
                        <TeamItem title={item.team.name} image={item.team.logo} />
                      </Link>
                    </div>
                  )
                })}
              </div>
            </Suspense>
          </div>

        </div>
      </div>
    </div>
  )
}