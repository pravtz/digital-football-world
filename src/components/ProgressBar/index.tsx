'use client'
import { useEffect, useState } from "react";
import * as Progress from '@radix-ui/react-progress';

type ProgressBarProps = {
    currentValueProgress: number;
    fullValueProgress: number
}
export const ProgressBar = ({ currentValueProgress, fullValueProgress }: ProgressBarProps) => {
    const [progress, setProgress] = useState(0);
    const [fullValue, setFullValue] = useState(100)

    useEffect(() => {
        setProgress(currentValueProgress)
        setFullValue(fullValueProgress)
    }, [currentValueProgress, fullValueProgress])

    return (

        <div className="w-fit">

            <Progress.Root className="relative bg-[#385657]/50 rounded-xl w-44 h-9 overflow-hidden border  border-white/10" value={progress}>
                <Progress.Indicator
                    className="bg-[#4F6B6C] w-full h-full transition-transform ease-in-out duration-1000 "
                    style={{ transform: `translateX(-${fullValue - progress}%)` }}
                />
            </Progress.Root>

            <p className=" text-sm text-center">{`${progress} / ${fullValue} requisições`}</p>

        </div>
    )
} 