import { fetchWrapper } from "@/utils/fetchWapper"
import { cookies } from "next/headers"

export type requestPlayer = {
    get: string,
    parameters: {
        id: string,
        season: string
        league?: string
    },
    errors: [],
    results: number,
    paging: {
        current: number,
        total: number
    },
    response: [
        {
            player: {
                id: number,
                name: string,
                firstname: string,
                lastname: string,
                age: number,
                birth: {
                    date: string,
                    place: string,
                    country: string
                },
                nationality: string,
                height: string,
                weight: string,
                injured: boolean,
                photo: string
            },
            statistics: [
                {
                    team: {
                        id: number,
                        name: string,
                        logo: string
                    },
                    league: {
                        id: number,
                        name: string,
                        country: string,
                        logo: string,
                        flag: string,
                        season: number
                    },
                    games: {
                        appearences: number,
                        lineups: number,
                        minutes: number,
                        number: null | number | string,
                        position: string,
                        rating: string,
                        captain: boolean
                    },
                    substitutes: {
                        in: number,
                        out: number,
                        bench: number
                    },
                    shots: {
                        total: null | number,
                        on: null | number | boolean | string
                    },
                    goals: {
                        total: number | null
                        conceded: number | null
                        assists: number | null
                        saves: number | null
                    },
                    passes: {
                        total: number | null
                        key: number | null
                        accuracy: number | null
                    },
                    tackles: {
                        total: null | number,
                        blocks: null | number,
                        interceptions: null | number
                    },
                    duels: {
                        total: number,
                        won: number
                    },
                    dribbles: {
                        attempts: null | number,
                        success: null | number,
                        past: null | number
                    },
                    fouls: {
                        drawn: null | number,
                        committed: null | number
                    },
                    cards: {
                        yellow: number,
                        yellowred: number,
                        red: number
                    },
                    penalty: {
                        won: null | number | string,
                        commited: null | number | string,
                        scored: number,
                        missed: number,
                        saved: number
                    }
                }
            ]
        }
    ]
}

const sugestionRevalidate = (3600 * 24 * 7) / 2 // twice a week


export async function getDataPlayer(id: string, season: string, revalidateTime: number = sugestionRevalidate) {
    const tokenKey = cookies().get('token')
    const data = await fetchWrapper<requestPlayer>('players?' + new URLSearchParams(
        {
            id,
            season
        }
    ), {
        headers: {
            'x-rapidapi-host': 'v3.football.api-sports.io',
            'x-rapidapi-key': `${tokenKey?.value}`
        },
        next: {
            revalidate: revalidateTime
        },
    })
    return data
}