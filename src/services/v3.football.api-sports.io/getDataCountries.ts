import { fetchWrapper } from "@/utils/fetchWapper";
import { cookies } from "next/headers";

export type countries = {
    "name": string
    "code": string | null
    "flag": string | null
}

export type requestCountry = {
    get: string
    parameters: [],
    errors: [],
    results: number,
    paging: {
        current: number,
        total: number
    },
    response: countries[]
}

const SugestionRevalidate = 60 * 60 * 24 * 7 //week

export async function getDataCountries(revalidateTime: number = SugestionRevalidate) {
    const tokenKey = cookies().get('token')

    const data = await fetchWrapper<requestCountry>('countries', {
        headers: {
            'x-rapidapi-host': 'v3.football.api-sports.io',
            'x-rapidapi-key': `${tokenKey?.value}`

        },
        next: {
            revalidate: revalidateTime,
        },

    })
    return data
}