import { fetchWrapper } from "@/utils/fetchWapper"
import { cookies } from "next/headers"

export type requestStatistics = {
  get: string,
  parameters: {
    team: string,
    season: string,
    league: string
  },
  errors: [],
  results: number,
  paging: {
    current: number,
    total: number
  },
  response: {
    league: {
      id: number,
      name: string,
      country: string,
      logo: string,
      flag: string,
      season: number
    },
    team: {
      id: number,
      name: string,
      logo: string
    },
    form: string,
    fixtures: {
      played: {
        home: number,
        away: number,
        total: number
      },
      wins: {
        home: number,
        away: number,
        total: number
      },
      draws: {
        home: number,
        away: number,
        total: number
      },
      loses: {
        home: number,
        away: number,
        total: number
      }
    },
    goals: {
      for: {
        total: {
          home: number,
          away: number,
          total: number
        },
        average: {
          home: string,
          away: string,
          total: string
        },
        minute: {
          "0-15": {
            total: number | null,
            percentage: string | null
          },
          "16-30": {
            total: number | null,
            percentage: string | null
          },
          "31-45": {
            total: number | null,
            percentage: string | null
          },
          "46-60": {
            total: number | null,
            percentage: string | null
          },
          "61-75": {
            total: number | null,
            percentage: string | null
          },
          "76-90": {
            total: number | null,
            percentage: string | null
          },
          "91-105": {
            total: number | null,
            percentage: string | null
          },
          "106-120": {
            total: number | null,
            percentage: string | null
          }
        }
      },
      against: {
        total: {
          home: number,
          away: number,
          total: number
        },
        average: {
          home: string,
          away: string,
          total: string
        },
        minute: {
          "0-15": {
            total: number | null,
            percentage: string | null
          },
          "16-30": {
            total: number | null,
            percentage: string | null
          },
          "31-45": {
            total: number | null,
            percentage: string | null
          },
          "46-60": {
            total: number | null,
            percentage: string | null
          },
          "61-75": {
            total: number | null,
            percentage: string | null
          },
          "76-90": {
            total: number | null,
            percentage: string | null
          },
          "91-105": {
            total: number | null,
            percentage: string | null
          },
          "106-120": {
            total: number | null,
            percentage: string | null
          }
        }
      }
    },
    biggest: {
      streak: {
        wins: number,
        draws: number,
        loses: number
      },
      wins: {
        home: number | string | null,
        away: number | string | null
      },
      loses: {
        home: number | string | null,
        away: number | string | null
      },
      goals: {
        for: {
          home: number | string | null,
          away: number | string | null
        },
        against: {
          home: number | string | null,
          away: number | string | null
        }
      }
    },
    clean_sheet: {
      home: number,
      away: number,
      total: number
    },
    failed_to_score: {
      home: number,
      away: number,
      total: number
    },
    penalty: {
      scored: {
        total: number,
        percentage: string
      },
      missed: {
        total: number,
        percentage: string
      },
      total: number
    },
    lineups: [
      {
        "formation": string,
        "played": number
      }
    ],
    cards: {
      yellow: {
        "0-15": {
          total: number | null,
          percentage: string | null
        },
        "16-30": {
          total: number | null,
          percentage: string | null
        },
        "31-45": {
          total: number | null,
          percentage: string | null
        },
        "46-60": {
          total: number | null,
          percentage: string | null
        },
        "61-75": {
          total: number | null,
          percentage: string | null
        },
        "76-90": {
          total: number | null,
          percentage: string | null
        },
        "91-105": {
          total: number | null,
          percentage: string | null
        },
        "106-120": {
          total: number | null,
          percentage: string | null
        }
      },
      red: {
        "0-15": {
          total: number | null,
          percentage: string | null
        },
        "16-30": {
          total: number | null,
          percentage: string | null
        },
        "31-45": {
          total: number | null,
          percentage: string | null
        },
        "46-60": {
          total: number | null,
          percentage: string | null
        },
        "61-75": {
          total: number | null,
          percentage: string | null
        },
        "76-90": {
          total: number | null,
          percentage: string | null
        },
        "91-105": {
          total: number | null,
          percentage: string | null
        },
        "106-120": {
          total: number | null,
          percentage: string | null
        }
      }
    }
  }
}

const sugestionRevalidate = (3600 * 24 * 7) / 2 // twice a week


export async function getDataStatistics(league: string, season: string, team: string, revalidateTime: number = sugestionRevalidate) {
  const tokenKey = cookies().get('token')
  const data = await fetchWrapper<requestStatistics>('teams/statistics?' + new URLSearchParams(
    {
      league,
      season,
      team
    }
  ), {
    headers: {
      'x-rapidapi-host': 'v3.football.api-sports.io',
      'x-rapidapi-key': `${tokenKey?.value}`
    },
    next: {
      revalidate: revalidateTime
    },
  })
  return data
}