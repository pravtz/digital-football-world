import { fetchWrapper } from "@/utils/fetchWapper"
import { cookies } from "next/headers"

export type requestLeagues = {
    get: string,
    parameters: {
        code: string,
        season: string
    },
    errors: [],
    results: number,
    paging: {
        current: number,
        total: number
    },
    response: [
        {
            league: {
                id: number,
                name: string,
                type: string,
                logo: string
            },
            country: {
                "name": string,
                "code": string,
                "flag": string
            },
            seasons: [
                {
                    year: number,
                    start: string,
                    end: string,
                    current: boolean,
                    coverage: {
                        fixtures: {
                            events: boolean,
                            lineups: boolean,
                            statistics_fixtures: boolean,
                            statistics_players: boolean
                        },
                        standings: boolean,
                        players: boolean,
                        top_scorers: boolean,
                        top_assists: boolean,
                        top_cards: boolean,
                        injuries: boolean,
                        predictions: boolean,
                        odds: boolean
                    }
                }
            ]
        }
    ]
}
const sugestionRevalidate = 3600 * 24 * 7 //total time of the week


export async function getDataLeagues(code: string, season: string, revalidateTime: number = sugestionRevalidate) {
    const tokenKey = cookies().get('token')
    const data = await fetchWrapper<requestLeagues>('leagues?' + new URLSearchParams(
        {
            code: code,
            season: season
        }
    ), {
        headers: {
            'x-rapidapi-host': 'v3.football.api-sports.io',
            'x-rapidapi-key': `${tokenKey?.value}`
        },
        next: {
            revalidate: revalidateTime
        },
    })

    return data
}