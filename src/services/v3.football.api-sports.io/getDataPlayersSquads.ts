import { fetchWrapper } from "@/utils/fetchWapper"
import { cookies } from "next/headers"

export type requestPlayersSquads = {
  get: string,
  parameters: {
    team: string
  },
  errors: [],
  results: number,
  paging: {
    current: number,
    total: number
  },
  response: [
    {
      team: {
        id: number,
        name: string,
        logo: string
      },
      players: [
        {
          id: number,
          name: string,
          age: number,
          number: number,
          position: string,
          photo: string
        },
      ]
    }
  ]
}

const sugestionRevalidate = (3600 * 24 * 7) / 2 // twice a week


export async function getDataPlayersSquads(team: string, revalidateTime: number = sugestionRevalidate) {
  const tokenKey = cookies().get('token')
  const data = await fetchWrapper<requestPlayersSquads>('players/squads?' + new URLSearchParams(
    {
      team
    }
  ), {
    headers: {
      'x-rapidapi-host': 'v3.football.api-sports.io',
      'x-rapidapi-key': `${tokenKey?.value}`
    },
    next: {
      revalidate: revalidateTime
    },
  })
  return data
}