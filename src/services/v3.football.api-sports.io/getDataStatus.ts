import { fetchWrapper } from '@/utils/fetchWapper'
import { cookies } from 'next/headers'


type requestStatus = {
    get: string,
    parameters: [],
    errors: [],
    results: number,
    paging: { current: number, total: number },
    response: {
        account: {
            fistname: string,
            lastname: string,
            email: string,
        },
        subscription: {
            plan: string,
            end: string,
            active: boolean
        },
        requests: {
            current: number
            limit_day: number
        }
    }
}

const SugestionRevalidateTime = 0

export async function getDataStatus(sugestionRevalidate: number = SugestionRevalidateTime) {
    const tokenKey = cookies().get('token')

    const data = await fetchWrapper<requestStatus>('status', {
        headers: {
            'x-rapidapi-host': 'v3.football.api-sports.io',
            'x-rapidapi-key': `${tokenKey?.value}`
        },
        cache: 'no-store'
    })
    return data
}
