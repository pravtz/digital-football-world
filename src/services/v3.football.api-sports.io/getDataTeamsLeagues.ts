import { fetchWrapper } from "@/utils/fetchWapper"
import { cookies } from "next/headers"

export type requestTeamsLeagues = {
  get: string,
  parameters: {
    season: string,
    league: string
  },
  errors: [],
  results: number,
  paging: {
    current: number,
    total: number
  },
  response: [
    {
      team: {
        id: number,
        name: string,
        code: string,
        country: string,
        founded: number,
        national: boolean,
        logo: string
      },
      venue: {
        id: number,
        name: string,
        address: string,
        city: string,
        capacity: number,
        surface: string,
        image: string
      }
    }
  ]
}
const sugestionRevalidate = (3600 * 24 * 7) / 2 // twice a week


export async function getDataTeamsLeagues(league: string, season: string, revalidateTime: number = sugestionRevalidate) {
  const tokenKey = cookies().get('token')
  const data = await fetchWrapper<requestTeamsLeagues>('teams?' + new URLSearchParams(
    {
      league,
      season,
    }
  ), {
    headers: {
      'x-rapidapi-host': 'v3.football.api-sports.io',
      'x-rapidapi-key': `${tokenKey?.value}`
    },
    next: {
      revalidate: revalidateTime
    },
  })
  return data
}