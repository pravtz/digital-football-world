import { fetchWrapper } from "@/utils/fetchWapper"
import { cookies } from "next/headers"

type requestLeaguesSeasons = {
    "get": string,
    "parameters": [],
    "errors": [],
    "results": number,
    "paging": {
        "current": number,
        "total": number
    },
    "response": number[]
}

const SugestionRevalidateTime = 60 * 60 * 24 * 7

export async function getDataLeaguesSeasons(sugestionRevalidate: number = SugestionRevalidateTime) {
    const tokenKey = cookies().get('token')

    const data = await fetchWrapper<requestLeaguesSeasons>('leagues/seasons', {
        headers: {
            'x-rapidapi-host': 'v3.football.api-sports.io',
            'x-rapidapi-key': `${tokenKey?.value}`
        },
        next: {
            revalidate: sugestionRevalidate
        }
    })
    return data
}
