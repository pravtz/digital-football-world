import { NextRequest, NextResponse } from 'next/server'


export async function middleware(request: NextRequest) {
    const { origin } = request.nextUrl

    let cookie = request.cookies.get('token')
    if (!cookie?.value) {

        return NextResponse.redirect(origin)
    }
    return NextResponse.next()
}

export const config = {
    matcher: '/home/:path*',
}