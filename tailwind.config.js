/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['var(--font-inter)']
      },
      colors: {
        'bg-default': '#314445',
        'text-default': '#CBDBDC',
        'primary': '#4F6B6C',
        'secondary': '#325355',
        'tertiary': '#44A890'

      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic': 'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
        'gradient-bg': ' linear-gradient(to bottom,transparent,rgb(var(--background-end-rgb)))'
      },
    },
  },
  plugins: [
    require("tailwindcss-radix")(),
    require('tailwind-scrollbar')({ nocompatible: true }),
  ],
}
